package ex2;

import javax.swing.*;

import java.awt.;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class ex2 {
    private JTextArea textArea1;
    private JTextField textField1;
    private JButton button1;
}

    public FunctieButon() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    textArea1.setText(" ");
                    String fileName =textField1.getText();
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
                    String a;
                    a = bufferedReader.readLine();
                    while(a != null){
                        textArea1.append(a+"\n");
                        a = bufferedReader.readLine();
                    }
                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }

package ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.io.*;
import java.util.*;

public class Ex2 extends JFrame {
    private JButton button1;
    private JTextField textField1;
    private JTextField textField2;


    Ex2() {

        setTitle("Exercitiul 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 200);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;


        textField1 = new JTextField();
        textField1.setBounds(70, 50, 150, 20);

        textField2 = new JTextField();
        textField1.setBounds(70, 50, 150, 20);

        button1 = new JButton("Citire");
        button1.setBounds(100,100,width, height);


        button1.addActionListener(new Scriere());

        add(textField1);
        add(button1);
        add(textField2);
    }
    public static void main(String[] args){
        new Ex2();
    }



    class Scriere implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            String filetext = Ex2.this.textField1.getText();
            try {
                File f = new File("D:\\Faculta\\ISP EXAMEN\\examen_isp_2020\\EXAMEN_RESTANTA\\src\\main\\java\\ex2\\fisier" + filetext + ".txt");
                Scanner input = new Scanner(f);
                String result = "";
                while (input.hasNextLine()) {
                    String fjala = input.next();
                    for (int i = fjala.length() - 1; i >= 0; i--) {
                        result += fjala.charAt(i);
                    }
                    result += " ";
                }
                input.close();
                System.out.print(result + " ");
            }
            catch (FileNotFoundException ae) {
                System.out.println("An error occurred.");
                ae.printStackTrace();
            }

        }
    }

}



